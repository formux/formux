const fs = require("fs");
const format = require("json-format");

const { resolve, join } = require("path");

const packages = (() => {
    const group = resolve("packages");
    
    const paths = [];
    for (const pkg of fs.readdirSync(group)) {
        paths.push(join(group, pkg, "package.json"));
    }

    return paths;
})();

const rootPkg = require("../package.json");

const KEEP_UPDATED = [
    "homepage", "bugs", "repository", "license", "author"
]

for (const pkg of packages) {
    const buffer = fs.readFileSync(pkg, "utf-8").toString();
    const json = JSON.parse(buffer);
    for (const info of KEEP_UPDATED) {
        json[info] = rootPkg[info];
    }
    fs.writeFileSync(pkg, format(json), { encoding: "utf-8" });
}