const fs = require("fs");
const rimraf = require("rimraf");

const { resolve, join } = require("path");
const { render } = require("sass");


const CSS_SASS_REGEX = /\.s?css$/;

function searchFiles(path = "./src", query = CSS_SASS_REGEX) {
    path = resolve(path);
    console.log(path)
    const entries = fs.readdirSync(path, { withFileTypes: true });

    const files = entries
        .filter(file => !file.isDirectory())
        .map(file => ({ ...file, path: join(path, file.name) }))
        .filter(({ path }) => query.test(path));

    const folders = entries.filter(folder => folder.isDirectory());

    for (const folder of folders)
        files.push(...getFiles(`${path}${folder.name}/`));

    return files;
}

render({
    file: resolve("src/formux.scss"),
    outFile: "lib/formux.css"
}, (err, { css }) => {
    if(err) throw Error(err);

    rimraf.sync("lib")
    if(!fs.existsSync("lib")) {
        fs.mkdirSync("lib")
    }

    fs.writeFileSync("lib/formux.css", css, { encoding: "utf-8" });
    
    for (const file of searchFiles()) {
        fs.copyFileSync(file.path, join("lib", file.name));
    };
});
